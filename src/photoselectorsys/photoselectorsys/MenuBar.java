package photoselectorsys;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class MenuBar extends JMenuBar {    
    private AppFrame listener;
    
    public MenuBar(){
		JMenu file = new JMenu("File");
		JMenu view = new JMenu("View");
		JMenu help = new JMenu("Help");
		JMenuItem newItem = new JMenuItem("New");
		JMenuItem openItem = new JMenuItem("Open...");
		JMenuItem exitItem = new JMenuItem("Exit");
		JMenuItem refreshItem = new JMenuItem("Refresh");
		JMenuItem aboutItem = new JMenuItem("About");
		
		// Setting up actionlisteners for the buttons
		newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuNew();
            }

        });
		
		openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuOpen();
            }
        });
		
		exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
		
		refreshItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                refresh();
            }
        });
		
		aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAbout();
            }
        });
		
		// Finalising the menubar
		file.add(newItem);
		file.add(openItem);
		file.addSeparator();
		file.add(exitItem);
		add(file);
		view.add(refreshItem);
		add(view);
		help.add(aboutItem);
		add(help);
	}

    public void menuNew(){
    	
    }
    public void menuOpen(){
    	
    }
    public void menuAbout(){
    	
    }
    public void refresh(){
    	
    }
    
	public AppFrame getListener() {
		return listener;
	}

	public void addListener(AppFrame listener) {
		this.listener = listener;
	}
}
