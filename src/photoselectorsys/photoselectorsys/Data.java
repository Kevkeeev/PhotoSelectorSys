package photoselectorsys;

import java.io.File;
import java.util.ArrayList;

public class Data {

	private String workingPath;
	private ArrayList<Picture> pictures;
	private ArrayList<String> subfolders;
	private int currentPicture;
	private AppFrame listener;
	
	public Data (String newPath){
		setPath(newPath);
		loadSubfolders();
		loadPictures();
		setIndex(0);
	}
	
	public void setPath(String newPath) {
		workingPath = newPath;
	}
	
	public String getPath(){
		return workingPath;
	}
	
	public void loadSubfolders(){
		subfolders = new ArrayList<String>();
		File dir = new File(getPath());
        File[] listFiles = dir.listFiles();
        for (File file : listFiles) {
            if (file.isDirectory()) {
                subfolders.add(file.getName());
            }
        }
        System.out.println("Done loading " + subfolders.size() + " subfolders!");
	}
	
	public void loadPictures(){ // only in this specific folder, not subfolders.
		pictures = new ArrayList<Picture>();
		File dir = new File(getPath());
        File[] listFiles = dir.listFiles();
        for (File file : listFiles) {
            if (
		        file.getName().toLowerCase().endsWith(".jpg")  ||
		        file.getName().toLowerCase().endsWith(".jpeg") ||
		        file.getName().toLowerCase().endsWith(".png")  ||
		        file.getName().toLowerCase().endsWith(".gif")
    		) {
                pictures.add( new Picture( workingPath, file.getName() ) );
            }
        }
        System.out.println("Done loading " + pictures.size() + " pictures!");
	}
	
	public void setIndex(int newIndex){
		currentPicture = newIndex;
	}
	
	public Picture getCurrentPicture(){
		return pictures.get(currentPicture);
	}
	
	public Picture getNextPicture(){
		currentPicture++;
		if(currentPicture >= pictures.size()) {
			currentPicture %= pictures.size();
		}
		return getCurrentPicture();
	}
	
	public Picture getPreviousPicture(){
		currentPicture--;
		if(currentPicture < 0) {
			currentPicture = pictures.size();
		}
		return getCurrentPicture();
	}
	
	public void sortPictures(){
		//define what to sort by
		//do the sorting
		//go to first image? same index (silly)? same picture?
	}
	
	public void movePicture(String destinationFolder) {
		getCurrentPicture().move(workingPath + "/" + destinationFolder);
		getNextPicture(); // advance to next picture
	}
	
	public int getNumPicturesLeft(){
		int returnvalue = 0;
		for(Picture pic : pictures) {
			if(pic.getPath() == workingPath) {
				returnvalue ++;
			}
		}
		return returnvalue;
	}
	
	public Boolean hasSubfolders(){
		return subfolders.size() > 0;
	}
	
	public ArrayList<String> getSubfolders(){
		return subfolders;
	}
	
	public void addListener(AppFrame listener) {
		this.listener = listener;
	}
	
	public AppFrame getListener(){
		return listener;
	}
}
