package photoselectorsys;

import javax.swing.JFileChooser;

public class JDirChooser {
  public static String openDirectory() {
    JFileChooser chooser = new JFileChooser();
    chooser.setCurrentDirectory(new java.io.File("."));
    chooser.setDialogTitle("Choose a photo from the map to sort:");
    chooser.setAcceptAllFileFilterUsed(true);

    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
    	String path = chooser.getCurrentDirectory().toString();
    	System.out.println("path = " + path);
        return(path);
    } else {
        System.exit(0); // By pressing ESC you can simply exit the program if you're done.
        return null; 
    }
  }
}
