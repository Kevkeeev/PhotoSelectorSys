package photoselectorsys;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MoveToPanel extends JPanel {
    private AppFrame listener;
    
    public MoveToPanel(int width, int height){
        setPreferredSize(new Dimension(width, height));
        setSize(width, height);
        setBorder(BorderFactory.createTitledBorder("MoveTo"));
        setVisible(true);
    }
        
    public void initButtons(){
        removeAll(); // removes any buttons present already. if there is nothing, there's nothing to remove.
        
        ArrayList<String> btns = new ArrayList<String>(listener.data.getSubfolders());

        btns.add("Add a new Button");
//        btns.add("Rotate CW");
//        btns.add("Rotate CCW");
//        btns.add("Rotate 180");
        
        MouseListener ml = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                    onButtonPress((JButton) arg0.getSource());
            }
            @Override
            public void mouseEntered(MouseEvent e)	{ /* unused */ }
            @Override
            public void mouseExited(MouseEvent e)	{ /* unused */ }
            @Override
            public void mousePressed(MouseEvent e)	{ /* unused */ }
            @Override
            public void mouseReleased(MouseEvent e)	{ /* unused */ }
        };
        
        JButton jb;
        if(listener.data.hasSubfolders()) { //if there's at least one folder to move toe. Else add one big button or ask to create subfolder
            String moved = listener.data.getCurrentPicture().checkIfInSubfolder(listener.data.getPath());
        	for(String btn : btns) {
        		jb = new JButton(btn);
        		if(moved.equals(btn) ) {
        			jb = new JButton("* " + btn);
        		} 
                jb.setFont(new Font("Arial", Font.PLAIN, 20));
                jb.setPreferredSize(new Dimension(this.getWidth()-20, 40));
                jb.setVisible(true);
                jb.addMouseListener(ml);            
                add(jb);
            }
        } else {
            jb = new JButton("<html><center>Add<br>a new<br>Button</center></html>");
            jb.setFont(new Font("Arial", Font.PLAIN, 55));
            jb.setPreferredSize(new Dimension(this.getWidth()-20, this.getHeight()-60));
            jb.setVisible(true);
            jb.addMouseListener(ml);            
            add(jb);
        }
        
        this.revalidate();
        this.repaint();
    }

    public void onButtonPress(JButton jb){
        String name = jb.getText();
        if(name.contains("*")) {
        	name = name.substring(2); //chop off the preceding "* "
        }
        switch (name) {
            case "Add a new Button":
            case "<html><center>Add<br>a new<br>Button</center></html>":
                listener.addButton();
                break;
            case "Rotate CW":
                listener.rotatePicture(0);
                break;
            case "Rotate CCW":
                listener.rotatePicture(1);
                break;
            case "Rotate 180":
                listener.rotatePicture(2);
                break;
            default:
                listener.movePicture(name);
                break;
        }
    }
    
    public void addListener(AppFrame listener) {
        this.listener = listener;
    }
}
