package photoselectorsys;

import java.awt.Dimension;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PicturePanel extends JPanel {    
    private AppFrame listener;
    
    public PicturePanel(int width, int height){
        setPreferredSize(new Dimension(width, height));
        setSize(width, height);
        setBorder(BorderFactory.createTitledBorder("Picture"));
        setVisible(true);
    }
    
    public void displayNextPicture() {
        Picture currentPicture = listener.data.getCurrentPicture();
        ImageIcon image = new ImageIcon(currentPicture.getPath() + File.separator + currentPicture.getName());
        System.out.println("current pic: " + currentPicture.getPath() + File.separator + currentPicture.getName());
        listener.setTitle("PhotoSelectorSys | " + currentPicture.getPath() + File.separator + currentPicture.getName() + " | " + listener.data.getNumPicturesLeft() + " pictures left!");
        JLabel imageLabel = new JLabel(image); 
        
        /**   FIX DISPLAY IMAGE HERE   **/
/*      if(
        	(image.getIconWidth() <= this.getWidth()) &&
        	(image.getIconHeight() <= this.getHeight())
        ) {
        	imageLabel.setBounds(0, 0, image.getIconWidth(), image.getIconHeight());
        } else  {
        	imageLabel.setBounds(100, 100, this.getWidth() - 100, this.getHeight() - 100);
        }
*/        
        int xOffset = 100;
        int yOffset = 100;
        int imgW = (image.getIconWidth() <= this.getWidth() - xOffset ? image.getIconWidth() : this.getWidth() - xOffset);
        int imgH = (image.getIconHeight() <= this.getHeight() - yOffset ? image.getIconHeight() : this.getHeight() - yOffset);
        
        imageLabel.setBounds(xOffset, yOffset, imgW, imgH);
        /**   FIX DISPLAY IMAGE HERE   **/        
        
        imageLabel.setVisible(true);
        removeAll(); //removes any previous pictures from the screen
        add(imageLabel);
        this.revalidate();
        this.repaint();        
    }

    public void addListener(AppFrame listener) {
        this.listener = listener;
    }
}
