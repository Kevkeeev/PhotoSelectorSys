package photoselectorsys;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Picture {
	private String path;
	private String name;
	
	public Picture(String Path, String Name){
		this.path = Path;
		this.name = Name;
	}
	
	public String getName(){
		return name;
	}
	
	public String getPath(){
		return path;
	}
	
	public void rotate(int degrees) {
		if(degrees == 90) {
			//clockwise
		} else if (degrees == 180) {
			//upside down
		} else if (degrees == 270) {
			//counterclockwise
		} else {
			System.err.println("Tried to rotate picture " /* + path + "/" */ + name + " an illegal " + degrees + " degrees!");
		}
	}
	
	/**
	 * 
	 * @param workingPath: the current path
	 * @return "" if in workingpath
	 * @return subfolder if in direct subfolder of workingpath
	 * @return path if not in workingpath or subfolder
	 */
	public String checkIfInSubfolder(String workingPath){
		if(workingPath == path) {
			return "";
		} else {
			if(path.startsWith(workingPath)){
				return path.substring(path.lastIndexOf(workingPath) + workingPath.length() + 1, path.length());
			} else {
				return path;
			}
		}
	}
	
	public void move(String destinationPath) {
		Path src = Paths.get(path + "/" + name );
        Path tgt = Paths.get(destinationPath + "/" + name);
        System.out.println("Moving picture " + name + " to " + destinationPath + ".");
        try {
            Files.move(src, tgt);
            this.path = destinationPath;
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

}
