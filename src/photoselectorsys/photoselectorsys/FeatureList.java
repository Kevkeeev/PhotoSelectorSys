//package photoselectorsys;
/*

done:
- grab correct folder
- check for subfolders
- ask for additional subfolders
- show picture centered top (keep regular size if smaller than panel)
- count num of pictures left
- for each subfolder show button
- also show buttons to rotate picture CW, CCW, UpsideDown.
- on buttonpress folder, move picture to subfolder, grab next picture
- if no subfolders present, buttonPanel becomes one big button
- on buttonpress folder, close picture
- if no pictures (left) in folder, show choose different folder.

busy:
- show picture full size centered middle (if it's larger than the panel, otherwise keep it its regular size)
- rotate picture CW, CCW, 180 degrees on buttonpress -> tricky, depends on file extensions how.

future wants:
- keybinds (below)
- settings (below)
- make set of pictures to cycle through
- sort set by imagename, (sub)folder, creation datetime, etc
- when moving picture, update picture location in set but keep in set (setting?) and show which folder it is in (button)
- Be able to cycle though set instead of first picture
- when moving already moved picture, move it from subfolder to different subfolder

Keybinds
- left/right: go to previous/next picture
- up/down: rotate picture CW/CCW
- 1..0: first 10 subfolders
- n = new subfolder
- s = select new set?
- r = reset set to current folder (only?)
- z = undo 

Settings:
- keybinds
- create default folders?
- if so, default folders names
- keep moved pictures in set?
- sorting style

 */