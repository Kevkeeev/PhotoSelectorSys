package photoselectorsys;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public final class AppFrame extends JFrame implements Observer {
	private final MenuBar menuBar;
	public final PicturePanel picturePanel;
	public final MoveToPanel moveToPanel;
    public Data data;

    public AppFrame() {
        super("PhotoSelectorSys | Loading...");
        
        /**   INIT APPFRAME   **/
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        int width = gd.getDisplayMode().getWidth();
        int height = gd.getDisplayMode().getHeight();
        //System.out.println("Dimension (WxH): " + width + " x " + height);
        setPreferredSize(new Dimension(width, height)); //says fuck you to preference
        setSize(width, height); //uses actual size
        
        setLocationRelativeTo(null); // this sets stuff in the center
        setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);
        
        /**   INIT PANELS & MENUBAR  **/      
        menuBar = new MenuBar();
        menuBar.addListener(this);
        picturePanel = new PicturePanel(width-210, height-80);
        picturePanel.addListener(this);
        moveToPanel = new MoveToPanel(210, height-80);
        moveToPanel.addListener(this);
                
        add(menuBar);
        add(picturePanel, BorderLayout.WEST);
        add(moveToPanel, BorderLayout.EAST);
        
        /**    INIT DATA   **/
        data = new Data(JDirChooser.openDirectory());
        
        moveToPanel.initButtons();
        picturePanel.displayNextPicture();
        
        //add frame picture   <-
            //loads picture from path sorted by name asc
        //add frame buttons ->
            //loads all folders in current path as folders
            //if no folders, ask to create folder
            //create button below folders buttons, to add additional folder
                //check: name not already in options arraylist (i.e. folder already exists)
                //if folder exists, cannot be created

    }

    public boolean isValidPicture(File file) {
	    return (
	        file.getName().toLowerCase().endsWith(".jpg")  ||
	        file.getName().toLowerCase().endsWith(".jpeg") ||
	        file.getName().toLowerCase().endsWith(".png")  ||
	        file.getName().toLowerCase().endsWith(".gif")
        );  
    }
    
    public void rotatePicture(int option) {
        //0 = CW
        //1 = CCW
        //2 = 180
    }
    
    public void movePicture(String name) {
    	data.movePicture(name);
        moveToPanel.initButtons();
        picturePanel.displayNextPicture();
    }
    
    public void addButton() {
        JTextField buttonName = new JTextField();
        Object[] options = { "New button's name:", buttonName };

        int option = JOptionPane.showConfirmDialog(null, options, "Add a Button...", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            String newButtonName = buttonName.getText();

            File f = new File(data.getPath() + "/" + newButtonName + "/");
            try{
                if(f.mkdirs()) { 
                    System.out.println(newButtonName + " directory created.");
                } else {
                    System.out.println(f.toString() + " directory is not created!");
                }
            } catch(Exception e){
                e.printStackTrace();
            } 

            moveToPanel.initButtons(); //revalidate the box with buttons
            this.revalidate();
        }
    }

    @Override
    public void update(Observable o, Object arg) { 
    	/* NO IDEA WHAT THIS DOES, BUT IT'S NECESSARY TO HAVE */ 
    }
}
